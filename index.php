<?php

require 'lib/db.php';
$db = new Outdoor_Database();

$menu_items = $db->getMenuItems();
$second_menu_items = $db->getSecondMenuItems();
$last_menu_items = $db->getLastMenuItems();

$events = $db->getPosts();
?><!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="css/main.css">
        <link href="https://fonts.googleapis.com/css?family=Lato:400,400i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Unica+One" rel="stylesheet">
        <script type="text/javascript" src="js/jquery-3.2.1.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
    </head>
    <body class="normal">
        <nav class="sidenavoverlay" id="sidenav">
            <span href="javascript:void(0)" class="closebtn" onclick="closenav()">×</span>
            <h3>Menu</h3>
            <ul class="filter_menu">
                <?php
                    foreach ($menu_items as $menu_item)
                    {
                        echo '<li>';
                        echo '<a href="javascript:void(0)" data-category="' . $menu_item['link'] . '">' . $menu_item['name'] . '</a>';
                        echo '</li>';
                    }
                ?>
            </ul>
            <h3>Laatste items</h3>
            <ul>
                <?php
                    foreach ($last_menu_items as $last_menu_item)
                    {
                        echo '<li>';
                        echo '<a href="/' . $last_menu_item['link'] . '" class="lastitem">' . $last_menu_item['name'] . '</a>';
                        echo '</li>';
                    }
                ?>
            </ul>
            <h3>OutdoorXL</h3>
            <ul>
                <?php
                    foreach ($second_menu_items as $second_menu_item)
                    {
                        echo '<li>';
                        echo '<a href="/' . $second_menu_item['link'] . '">' . $second_menu_item['name'] . '</a>';
                        echo '</li>';
                    }
                ?>
            </ul>
        </nav>
        <div id="container">
            <div class="top" id="top">
                <img class="toplogo" src="logo.png">
                <div class="navbtn" onclick="opennav();">&#9776;</div>
            </div>
            <div class="events">
                <?php
                    $i = 2;
                    $event_images = count($events);
                    foreach ($events as $event)
                    {
                            $i++;
                            $visibility_class = '';
                            if($i < 6)
                            {
                                $visibility_class = 'event_active';
                            }
                            echo '<div class="event_holder ' . $visibility_class . ' ' . $event['category'] . '" id="foto' . $i . '">';
                            echo '<img src="' . $event['image'] . '" class="eventimg">';
                            echo '<div class="eventtxt">';
                            echo '<p class="eventtxtl"><a href="' . $event['link'] . '">' . $event['large_text'] . '</a></p>';
                            echo '<p class="eventtxtm"><a href="' . $event['link'] . '">' . $event['small_text'] . '</a></p>';
                            echo '<p class="eventtxts"><a href="' . $event['link'] . '">' . $event['date'] . '</a></p>';
                            echo '</div>';
                            echo '</div>';
                    }
                    echo '<script>var event_images = ' . $i . ';</script>';
                    ?>
            </div>
            <div class="bottompart">
                <div class="spacing"></div>
                <div class="loadmorebtn" onclick="loadmore()" id="loadmore">load more</div>
            </div>
        </div>
    </body>
</html>