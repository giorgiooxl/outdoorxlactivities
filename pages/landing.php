<?php

require '../lib/db.php';
$db = new Outdoor_Database();

$menu_items = $db->getMenuItems();
$second_menu_items = $db->getSecondMenuItems();
$last_menu_items = $db->getLastMenuItems();

$stories = $db->getStories();

$page_request = $_GET['page_request'];

$event = $page_request;

?><!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/landingpage.css">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> 
        <link href="https://fonts.googleapis.com/css?family=Zilla+Slab" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lato:400,400i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Unica+One" rel="stylesheet"> 
        <link rel="stylesheet" href="css/font-awesome-4.7.0/font-awesome-4.7.0/css/font-awesome.css">
        <script type="text/javascript" src="js/jquery-3.2.1.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
    </head>
    <body class="normal">
        <nav class="sidenavoverlay" id="sidenav">
            <span href="javascript:void(0)" class="closebtn" onclick="closenav()">×</span>
            <h3>Menu</h3>
            <ul class="filter_menu">
                <?php
                    foreach ($menu_items as $menu_item)
                    {
                        echo '<li>';
                        echo '<a href="javascript:void(0)" data-category="' . $menu_item['link'] . '">' . $menu_item['name'] . '</a>';
                        echo '</li>';
                    }
                ?>
            </ul>
            <h3>Laatste items</h3>
            <ul>
                <?php
                    foreach ($last_menu_items as $last_menu_item)
                    {
                        echo '<li>';
                        echo '<a href="/' . $last_menu_item['link'] . '" class="lastitem">' . $last_menu_item['name'] . '</a>';
                        echo '</li>';
                    }
                ?>
            </ul>
            <h3>OutdoorXL</h3>
            <ul>
                <?php
                    foreach ($second_menu_items as $second_menu_item)
                    {
                        echo '<li>';
                        echo '<a href="/' . $second_menu_item['link'] . '">' . $second_menu_item['name'] . '</a>';
                        echo '</li>';
                    }
                ?>
            </ul>
        </nav>
        <div id="container">
            <div class="top" id="top">
                <a href="index.php"><img class="toplogo" src="logo.png"></a>
                <div class="navbtn" onclick="opennav();">&#9776;</div>
            </div>
            <div id="story">
                <?php
                    echo '<img src="' . $stories[$event]['image'] . '" class="storyimg">';
                    echo '<div class="storybntxt">';
                    echo '<p class="storybntxtl">' . $stories[$event]['large_text'] . '</p>';
                    echo '<p class="storybntxtm">' . $stories[$event]['small_text'] . '</p>';
                    echo '<p class="storybntxts">' . $stories[$event]['date'] . '</p>';
                    echo '</div>';
                    echo '</div>';
                ?>
                <div class="storytext">
                    <?php
                        echo '<div id="summary">';
                        echo '<h3>' . $stories[$event]['summary'] . '</h3>';
                        echo '<div class="sharebtns">';
                        echo '<a href="#"><i class="fa fa-facebook"></i></a>';
                        echo '<a href="#"><i class="fa fa-twitter"></i></a>';
                        echo '<a href="#"><i class="fa fa-google-plus"></i></a>';
                        echo '<p class="readtime"><i class="fa fa-clock-o" aria-hidden="true"></i>
 ' . $stories[$event]['readtime'] . ' minuten leestijd</p>';
                        echo '</div>';
                        echo '</div>';
                        echo '<div id="smallspacing"></div>';
                        echo '<h2> - ' . $stories[$event]['firsthead'] . ' - </h2>';
                        echo '<p>' . $stories[$event]['firstbody'] . '</p>';
                        echo '<h2> - ' . $stories[$event]['secondhead'] . ' - </h2>';
                        echo '<p>' . $stories[$event]['secondbody'] . '</p>';
                        echo '<h2> - ' . $stories[$event]['thirdhead'] . ' - </h2>';
                        echo '<p>' . $stories[$event]['thirdbody'] . '</p>';
                    ?>
                </div>
                <div class="spacing"></div>
            </div>
        </div>
    </body>
</html>