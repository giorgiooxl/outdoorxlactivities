<?php 

Class Outdoor_Database
{
    protected $db; 
    protected $lang = 'NL'; 
    
    public function __construct()
    {
           
    }
    
    public function getMenuItems()
    {
           // $this->db->query("SELECT * FROM menu_items WHERE active = 1 ORDER BY position ASC");
           return $this->getMenuItemsTmp();
    }
    
    public function getSecondMenuItems()
    {
           // $this->db->query("SELECT * FROM menu_items WHERE active = 1 ORDER BY position ASC");
           return $this->getSecondMenuItemsTmp();
    }
    
    public function getLastMenuItems()
    {
           // $this->db->query("SELECT * FROM menu_items WHERE active = 1 ORDER BY position ASC");
           return $this->getLastMenuItemsTmp();
    }
    
    public function getPosts()
    {
           // $this->db->query("SELECT * FROM posts WHERE active = 1 ORDER BY date_created DESC");
           return $this->getPostsTmp();
    }
    
    public function getStories()
    {
           // $this->db->query("SELECT * FROM posts WHERE active = 1 ORDER BY date_created DESC");
           return $this->getStoriesTmp();
    }
    
    /* TEMPORARY FUNCTIONS BEFORE WE HAVE A DATABASE */
    
    public function getMenuItemsTmp()
    {
        $menu_items = array();
        
        $menu_items[] = array(
            'name'  => 'Skiën',
            'link'  => 'skien' 
        );
        $menu_items[] = array(
            'name'  => 'Varen',
            'link'  => 'varen' 
        );
        $menu_items[] = array(
            'name'  => 'Wandelen',
            'link'  => 'wandelen' 
        );
        $menu_items[] = array(
            'name'  => 'Trailrunning',
            'link'  => 'trailrunning' 
        );
        $menu_items[] = array(
            'name'  => 'Fietsen',
            'link'  => 'fietsen' 
        );
        
        return $menu_items;
    }
    public function getSecondMenuItemsTmp()
    {
        $second_menu_items = array();
        
        $second_menu_items[] = array(
            'name'  => 'Skies',
            'link'  => 'skies' 
        );
        $second_menu_items[] = array(
            'name'  => 'Boten',
            'link'  => 'boten' 
        );
        $second_menu_items[] = array(
            'name'  => 'Tenten',
            'link'  => 'tenten' 
        );
        $second_menu_items[] = array(
            'name'  => 'OutdoorXL winkel',
            'link'  => 'home' 
        );
        $second_menu_items[] = array(
            'name'  => 'Nieuws',
            'link'  => 'nieuws' 
        );
        
        return $second_menu_items;
    }
    public function getLastMenuItemsTmp()
    {
        $last_menu_items = array();
        
        $last_menu_items[] = array(
            'name'  => 'Viking Unlimited Kunststof Klap',
            'link'  => 'viking-unlimited-kunststof-klap'
        );
        
        return $last_menu_items;
    }
    public function getPostsTmp()
    {
        $events = Array(
            0 => array(
                "large_text" => "Dit is een grote tekst",
                "small_text" => "Dit is een kleine tekst",
                "date" => "29 juni 2017",
                "category" => "wandelen",
                "link" => "/wandelen",
                "image" => "images/foto1.jpg"
            ),
            1 => array(
                "large_text" => "Dit is een grote tekst",
                "small_text" => "Dit is een kleine tekst",
                "date" => "29 juni 2017",
                "category" => "wandelen",
                "link" => "/wandelen",
                "image" => "images/foto2.jpg"
            ),
            2 => array(
                "large_text" => "Dit is een grote tekst",
                "small_text" => "Dit is een kleine tekst",
                "date" => "29 juni 2017",
                "category" => "trailrunning",
                "link" => "/trailrunning",
                "image" => "images/foto3.jpg"
            ),
            3 => array(
                "large_text" => "Dit is een grote tekst",
                "small_text" => "Dit is een kleine tekst",
                "date" => "29 juni 2017",
                "category" => "trailrunning",
                "link" => "/trailrunning",
                "image" => "images/foto4.jpg"
            ),
            4 => array(
                "large_text" => "Dit is een grote tekst",
                "small_text" => "Dit is een kleine tekst",
                "date" => "29 juni 2017",
                "category" => "skien",
                "link" => "/skien",
                "image" => "images/foto5.jpg"
            ),
            5 => array(
                "large_text" => "Dit is een grote tekst",
                "small_text" => "Dit is een kleine tekst",
                "date" => "29 juni 2017",
                "category" => "fietsen",
                "link" => "/fietsen",
                "image" => "images/foto6.jpg"
            ),
            6 => array(
                "large_text" => "Dit is een grote tekst",
                "small_text" => "Dit is een kleine tekst",
                "date" => "29 juni 2017",
                "category" => "wandelen",
                "link" => "/wandelen",
                "image" => "images/foto7.jpg"
            ),
            7 => array(
                "large_text" => "Dit is een grote tekst",
                "small_text" => "Dit is een kleine tekst",
                "date" => "29 juni 2017",
                "category" => "fietsen",
                "link" => "/fietsen",
                "image" => "images/foto8.jpg"
            ),
            8 => array(
                "large_text" => "Dit is een grote tekst",
                "small_text" => "Dit is een kleine tekst",
                "date" => "29 juni 2017",
                "category" => "varen",
                "link" => "/varen",
                "image" => "images/foto9.jpg"
            ),
        );
        return $events;
    }
    public function getStoriesTmp()
    {
        $stories = Array(
            0 => array(
                "large_text" => "Dit is een grote tekst",
                "small_text" => "Dit is een kleine tekst",
                "date" => "29 juni 2017",
                "category" => "wandelen",
                "link" => "/wandelen",
                "image" => "images/foto1.jpg",
                
                "summary" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam mauris urna, pellentesque vitae nisi at, efficitur placerat dolor. Curabitur at felis vestibulum, faucibus mi quis, consequat est. Curabitur hendrerit suscipit ante tempus sollicitudin. Aenean sed egestas est.",
                "readtime" => "2",
                "firsthead" => "Lorem",
                "firstbody" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dignissim sollicitudin dui ac vehicula. Integer id nibh sed ex rutrum condimentum. Duis a bibendum quam. Suspendisse maximus varius commodo. Nulla congue dapibus libero vitae efficitur. Maecenas molestie lorem massa, et luctus velit semper vel. Etiam faucibus tellus a lacus vehicula pulvinar. Vestibulum velit lacus, egestas ornare purus eu, feugiat hendrerit ipsum. Pellentesque venenatis sed arcu vel vulputate. Curabitur vitae scelerisque metus. Curabitur pretium sollicitudin felis eu commodo.",
                "secondhead" => "Ipsum",
                "secondbody" => "Donec fermentum, tortor sed rhoncus vehicula, metus nibh lobortis mauris, a fermentum sem erat ac mi. Etiam sit amet rutrum nisi. Etiam placerat elit ac sollicitudin rutrum. Etiam elit turpis, interdum nec orci vel, mattis gravida lectus. Proin tempus, tellus vitae vulputate placerat, dolor arcu lobortis lectus, congue lobortis ante mi sed augue. Vivamus orci orci, pharetra nec leo non, posuere posuere mi. Quisque auctor odio sit amet est porttitor ornare. Suspendisse semper efficitur ultricies. Pellentesque faucibus enim ac ligula accumsan vulputate.",
                "thirdhead" => "Dolor",
                "thirdbody" => "Nam sollicitudin ante vel nibh eleifend fringilla. Phasellus ex ligula, consequat sit amet cursus semper, consequat ac dolor. Duis ultricies sem enim, quis euismod sapien ultrices id. Praesent et ipsum at leo congue rutrum. Vestibulum at velit et nisi interdum eleifend ac in nibh. Donec mauris dui, pulvinar sed quam id, scelerisque volutpat risus. Nunc quis scelerisque nulla, at dignissim enim. Duis eros lectus, scelerisque in imperdiet vitae, luctus vel arcu. Ut hendrerit lorem vel purus rhoncus dictum. Fusce scelerisque consequat metus nec blandit."
            ),
            1 => array(
                "large_text" => "Dit is een grote tekst",
                "small_text" => "Dit is een kleine tekst",
                "date" => "29 juni 2017",
                "category" => "wandelen",
                "link" => "/wandelen",
                "image" => "images/foto2.jpg",
                
                "summary" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam mauris urna, pellentesque vitae nisi at, efficitur placerat dolor. Curabitur at felis vestibulum, faucibus mi quis, consequat est. Curabitur hendrerit suscipit ante tempus sollicitudin. Aenean sed egestas est.",
                "readtime" => "2",
                "firsthead" => "Lorem",
                "firstbody" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dignissim sollicitudin dui ac vehicula. Integer id nibh sed ex rutrum condimentum. Duis a bibendum quam. Suspendisse maximus varius commodo. Nulla congue dapibus libero vitae efficitur. Maecenas molestie lorem massa, et luctus velit semper vel. Etiam faucibus tellus a lacus vehicula pulvinar. Vestibulum velit lacus, egestas ornare purus eu, feugiat hendrerit ipsum. Pellentesque venenatis sed arcu vel vulputate. Curabitur vitae scelerisque metus. Curabitur pretium sollicitudin felis eu commodo.",
                "secondhead" => "Ipsum",
                "secondbody" => "Donec fermentum, tortor sed rhoncus vehicula, metus nibh lobortis mauris, a fermentum sem erat ac mi. Etiam sit amet rutrum nisi. Etiam placerat elit ac sollicitudin rutrum. Etiam elit turpis, interdum nec orci vel, mattis gravida lectus. Proin tempus, tellus vitae vulputate placerat, dolor arcu lobortis lectus, congue lobortis ante mi sed augue. Vivamus orci orci, pharetra nec leo non, posuere posuere mi. Quisque auctor odio sit amet est porttitor ornare. Suspendisse semper efficitur ultricies. Pellentesque faucibus enim ac ligula accumsan vulputate.",
                "thirdhead" => "Dolor",
                "thirdbody" => "Nam sollicitudin ante vel nibh eleifend fringilla. Phasellus ex ligula, consequat sit amet cursus semper, consequat ac dolor. Duis ultricies sem enim, quis euismod sapien ultrices id. Praesent et ipsum at leo congue rutrum. Vestibulum at velit et nisi interdum eleifend ac in nibh. Donec mauris dui, pulvinar sed quam id, scelerisque volutpat risus. Nunc quis scelerisque nulla, at dignissim enim. Duis eros lectus, scelerisque in imperdiet vitae, luctus vel arcu. Ut hendrerit lorem vel purus rhoncus dictum. Fusce scelerisque consequat metus nec blandit."
            ),
            2 => array(
                "large_text" => "Dit is een grote tekst",
                "small_text" => "Dit is een kleine tekst",
                "date" => "29 juni 2017",
                "category" => "trailrunning",
                "link" => "/trailrunning",
                "image" => "images/foto3.jpg",
                
                "summary" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam mauris urna, pellentesque vitae nisi at, efficitur placerat dolor. Curabitur at felis vestibulum, faucibus mi quis, consequat est. Curabitur hendrerit suscipit ante tempus sollicitudin. Aenean sed egestas est.",
                "readtime" => "2",
                "firsthead" => "Lorem",
                "firstbody" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dignissim sollicitudin dui ac vehicula. Integer id nibh sed ex rutrum condimentum. Duis a bibendum quam. Suspendisse maximus varius commodo. Nulla congue dapibus libero vitae efficitur. Maecenas molestie lorem massa, et luctus velit semper vel. Etiam faucibus tellus a lacus vehicula pulvinar. Vestibulum velit lacus, egestas ornare purus eu, feugiat hendrerit ipsum. Pellentesque venenatis sed arcu vel vulputate. Curabitur vitae scelerisque metus. Curabitur pretium sollicitudin felis eu commodo.",
                "secondhead" => "Ipsum",
                "secondbody" => "Donec fermentum, tortor sed rhoncus vehicula, metus nibh lobortis mauris, a fermentum sem erat ac mi. Etiam sit amet rutrum nisi. Etiam placerat elit ac sollicitudin rutrum. Etiam elit turpis, interdum nec orci vel, mattis gravida lectus. Proin tempus, tellus vitae vulputate placerat, dolor arcu lobortis lectus, congue lobortis ante mi sed augue. Vivamus orci orci, pharetra nec leo non, posuere posuere mi. Quisque auctor odio sit amet est porttitor ornare. Suspendisse semper efficitur ultricies. Pellentesque faucibus enim ac ligula accumsan vulputate.",
                "thirdhead" => "Dolor",
                "thirdbody" => "Nam sollicitudin ante vel nibh eleifend fringilla. Phasellus ex ligula, consequat sit amet cursus semper, consequat ac dolor. Duis ultricies sem enim, quis euismod sapien ultrices id. Praesent et ipsum at leo congue rutrum. Vestibulum at velit et nisi interdum eleifend ac in nibh. Donec mauris dui, pulvinar sed quam id, scelerisque volutpat risus. Nunc quis scelerisque nulla, at dignissim enim. Duis eros lectus, scelerisque in imperdiet vitae, luctus vel arcu. Ut hendrerit lorem vel purus rhoncus dictum. Fusce scelerisque consequat metus nec blandit."
            ),
            3 => array(
                "large_text" => "Dit is een grote tekst",
                "small_text" => "Dit is een kleine tekst",
                "date" => "29 juni 2017",
                "category" => "trailrunning",
                "link" => "/trailrunning",
                "image" => "images/foto4.jpg",
                
                "summary" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam mauris urna, pellentesque vitae nisi at, efficitur placerat dolor. Curabitur at felis vestibulum, faucibus mi quis, consequat est. Curabitur hendrerit suscipit ante tempus sollicitudin. Aenean sed egestas est.",
                "readtime" => "2",
                "firsthead" => "Lorem",
                "firstbody" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dignissim sollicitudin dui ac vehicula. Integer id nibh sed ex rutrum condimentum. Duis a bibendum quam. Suspendisse maximus varius commodo. Nulla congue dapibus libero vitae efficitur. Maecenas molestie lorem massa, et luctus velit semper vel. Etiam faucibus tellus a lacus vehicula pulvinar. Vestibulum velit lacus, egestas ornare purus eu, feugiat hendrerit ipsum. Pellentesque venenatis sed arcu vel vulputate. Curabitur vitae scelerisque metus. Curabitur pretium sollicitudin felis eu commodo.",
                "secondhead" => "Ipsum",
                "secondbody" => "Donec fermentum, tortor sed rhoncus vehicula, metus nibh lobortis mauris, a fermentum sem erat ac mi. Etiam sit amet rutrum nisi. Etiam placerat elit ac sollicitudin rutrum. Etiam elit turpis, interdum nec orci vel, mattis gravida lectus. Proin tempus, tellus vitae vulputate placerat, dolor arcu lobortis lectus, congue lobortis ante mi sed augue. Vivamus orci orci, pharetra nec leo non, posuere posuere mi. Quisque auctor odio sit amet est porttitor ornare. Suspendisse semper efficitur ultricies. Pellentesque faucibus enim ac ligula accumsan vulputate.",
                "thirdhead" => "Dolor",
                "thirdbody" => "Nam sollicitudin ante vel nibh eleifend fringilla. Phasellus ex ligula, consequat sit amet cursus semper, consequat ac dolor. Duis ultricies sem enim, quis euismod sapien ultrices id. Praesent et ipsum at leo congue rutrum. Vestibulum at velit et nisi interdum eleifend ac in nibh. Donec mauris dui, pulvinar sed quam id, scelerisque volutpat risus. Nunc quis scelerisque nulla, at dignissim enim. Duis eros lectus, scelerisque in imperdiet vitae, luctus vel arcu. Ut hendrerit lorem vel purus rhoncus dictum. Fusce scelerisque consequat metus nec blandit."
            ),
            4 => array(
                "large_text" => "Dit is een grote tekst",
                "small_text" => "Dit is een kleine tekst",
                "date" => "29 juni 2017",
                "category" => "skien",
                "link" => "/skien",
                "image" => "images/foto5.jpg",
                
                "summary" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam mauris urna, pellentesque vitae nisi at, efficitur placerat dolor. Curabitur at felis vestibulum, faucibus mi quis, consequat est. Curabitur hendrerit suscipit ante tempus sollicitudin. Aenean sed egestas est.",
                "readtime" => "2",
                "firsthead" => "Lorem",
                "firstbody" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dignissim sollicitudin dui ac vehicula. Integer id nibh sed ex rutrum condimentum. Duis a bibendum quam. Suspendisse maximus varius commodo. Nulla congue dapibus libero vitae efficitur. Maecenas molestie lorem massa, et luctus velit semper vel. Etiam faucibus tellus a lacus vehicula pulvinar. Vestibulum velit lacus, egestas ornare purus eu, feugiat hendrerit ipsum. Pellentesque venenatis sed arcu vel vulputate. Curabitur vitae scelerisque metus. Curabitur pretium sollicitudin felis eu commodo.",
                "secondhead" => "Ipsum",
                "secondbody" => "Donec fermentum, tortor sed rhoncus vehicula, metus nibh lobortis mauris, a fermentum sem erat ac mi. Etiam sit amet rutrum nisi. Etiam placerat elit ac sollicitudin rutrum. Etiam elit turpis, interdum nec orci vel, mattis gravida lectus. Proin tempus, tellus vitae vulputate placerat, dolor arcu lobortis lectus, congue lobortis ante mi sed augue. Vivamus orci orci, pharetra nec leo non, posuere posuere mi. Quisque auctor odio sit amet est porttitor ornare. Suspendisse semper efficitur ultricies. Pellentesque faucibus enim ac ligula accumsan vulputate.",
                "thirdhead" => "Dolor",
                "thirdbody" => "Nam sollicitudin ante vel nibh eleifend fringilla. Phasellus ex ligula, consequat sit amet cursus semper, consequat ac dolor. Duis ultricies sem enim, quis euismod sapien ultrices id. Praesent et ipsum at leo congue rutrum. Vestibulum at velit et nisi interdum eleifend ac in nibh. Donec mauris dui, pulvinar sed quam id, scelerisque volutpat risus. Nunc quis scelerisque nulla, at dignissim enim. Duis eros lectus, scelerisque in imperdiet vitae, luctus vel arcu. Ut hendrerit lorem vel purus rhoncus dictum. Fusce scelerisque consequat metus nec blandit."
            ),
            5 => array(
                "large_text" => "Dit is een grote tekst",
                "small_text" => "Dit is een kleine tekst",
                "date" => "29 juni 2017",
                "category" => "fietsen",
                "link" => "/fietsen",
                "image" => "images/foto6.jpg",
                
                "summary" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam mauris urna, pellentesque vitae nisi at, efficitur placerat dolor. Curabitur at felis vestibulum, faucibus mi quis, consequat est. Curabitur hendrerit suscipit ante tempus sollicitudin. Aenean sed egestas est.",
                "readtime" => "2",
                "firsthead" => "Lorem",
                "firstbody" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dignissim sollicitudin dui ac vehicula. Integer id nibh sed ex rutrum condimentum. Duis a bibendum quam. Suspendisse maximus varius commodo. Nulla congue dapibus libero vitae efficitur. Maecenas molestie lorem massa, et luctus velit semper vel. Etiam faucibus tellus a lacus vehicula pulvinar. Vestibulum velit lacus, egestas ornare purus eu, feugiat hendrerit ipsum. Pellentesque venenatis sed arcu vel vulputate. Curabitur vitae scelerisque metus. Curabitur pretium sollicitudin felis eu commodo.",
                "secondhead" => "Ipsum",
                "secondbody" => "Donec fermentum, tortor sed rhoncus vehicula, metus nibh lobortis mauris, a fermentum sem erat ac mi. Etiam sit amet rutrum nisi. Etiam placerat elit ac sollicitudin rutrum. Etiam elit turpis, interdum nec orci vel, mattis gravida lectus. Proin tempus, tellus vitae vulputate placerat, dolor arcu lobortis lectus, congue lobortis ante mi sed augue. Vivamus orci orci, pharetra nec leo non, posuere posuere mi. Quisque auctor odio sit amet est porttitor ornare. Suspendisse semper efficitur ultricies. Pellentesque faucibus enim ac ligula accumsan vulputate.",
                "thirdhead" => "Dolor",
                "thirdbody" => "Nam sollicitudin ante vel nibh eleifend fringilla. Phasellus ex ligula, consequat sit amet cursus semper, consequat ac dolor. Duis ultricies sem enim, quis euismod sapien ultrices id. Praesent et ipsum at leo congue rutrum. Vestibulum at velit et nisi interdum eleifend ac in nibh. Donec mauris dui, pulvinar sed quam id, scelerisque volutpat risus. Nunc quis scelerisque nulla, at dignissim enim. Duis eros lectus, scelerisque in imperdiet vitae, luctus vel arcu. Ut hendrerit lorem vel purus rhoncus dictum. Fusce scelerisque consequat metus nec blandit."
            ),
            6 => array(
                "large_text" => "Dit is een grote tekst",
                "small_text" => "Dit is een kleine tekst",
                "date" => "29 juni 2017",
                "category" => "wandelen",
                "link" => "/wandelen",
                "image" => "images/foto7.jpg",
                
                "summary" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam mauris urna, pellentesque vitae nisi at, efficitur placerat dolor. Curabitur at felis vestibulum, faucibus mi quis, consequat est. Curabitur hendrerit suscipit ante tempus sollicitudin. Aenean sed egestas est.",
                "readtime" => "2",
                "firsthead" => "Lorem",
                "firstbody" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dignissim sollicitudin dui ac vehicula. Integer id nibh sed ex rutrum condimentum. Duis a bibendum quam. Suspendisse maximus varius commodo. Nulla congue dapibus libero vitae efficitur. Maecenas molestie lorem massa, et luctus velit semper vel. Etiam faucibus tellus a lacus vehicula pulvinar. Vestibulum velit lacus, egestas ornare purus eu, feugiat hendrerit ipsum. Pellentesque venenatis sed arcu vel vulputate. Curabitur vitae scelerisque metus. Curabitur pretium sollicitudin felis eu commodo.",
                "secondhead" => "Ipsum",
                "secondbody" => "Donec fermentum, tortor sed rhoncus vehicula, metus nibh lobortis mauris, a fermentum sem erat ac mi. Etiam sit amet rutrum nisi. Etiam placerat elit ac sollicitudin rutrum. Etiam elit turpis, interdum nec orci vel, mattis gravida lectus. Proin tempus, tellus vitae vulputate placerat, dolor arcu lobortis lectus, congue lobortis ante mi sed augue. Vivamus orci orci, pharetra nec leo non, posuere posuere mi. Quisque auctor odio sit amet est porttitor ornare. Suspendisse semper efficitur ultricies. Pellentesque faucibus enim ac ligula accumsan vulputate.",
                "thirdhead" => "Dolor",
                "thirdbody" => "Nam sollicitudin ante vel nibh eleifend fringilla. Phasellus ex ligula, consequat sit amet cursus semper, consequat ac dolor. Duis ultricies sem enim, quis euismod sapien ultrices id. Praesent et ipsum at leo congue rutrum. Vestibulum at velit et nisi interdum eleifend ac in nibh. Donec mauris dui, pulvinar sed quam id, scelerisque volutpat risus. Nunc quis scelerisque nulla, at dignissim enim. Duis eros lectus, scelerisque in imperdiet vitae, luctus vel arcu. Ut hendrerit lorem vel purus rhoncus dictum. Fusce scelerisque consequat metus nec blandit."
            ),
            7 => array(
                "large_text" => "Dit is een grote tekst",
                "small_text" => "Dit is een kleine tekst",
                "date" => "29 juni 2017",
                "category" => "fietsen",
                "link" => "/fietsen",
                "image" => "images/foto8.jpg",
                
                "summary" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam mauris urna, pellentesque vitae nisi at, efficitur placerat dolor. Curabitur at felis vestibulum, faucibus mi quis, consequat est. Curabitur hendrerit suscipit ante tempus sollicitudin. Aenean sed egestas est.",
                "readtime" => "2",
                "firsthead" => "Lorem",
                "firstbody" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dignissim sollicitudin dui ac vehicula. Integer id nibh sed ex rutrum condimentum. Duis a bibendum quam. Suspendisse maximus varius commodo. Nulla congue dapibus libero vitae efficitur. Maecenas molestie lorem massa, et luctus velit semper vel. Etiam faucibus tellus a lacus vehicula pulvinar. Vestibulum velit lacus, egestas ornare purus eu, feugiat hendrerit ipsum. Pellentesque venenatis sed arcu vel vulputate. Curabitur vitae scelerisque metus. Curabitur pretium sollicitudin felis eu commodo.",
                "secondhead" => "Ipsum",
                "secondbody" => "Donec fermentum, tortor sed rhoncus vehicula, metus nibh lobortis mauris, a fermentum sem erat ac mi. Etiam sit amet rutrum nisi. Etiam placerat elit ac sollicitudin rutrum. Etiam elit turpis, interdum nec orci vel, mattis gravida lectus. Proin tempus, tellus vitae vulputate placerat, dolor arcu lobortis lectus, congue lobortis ante mi sed augue. Vivamus orci orci, pharetra nec leo non, posuere posuere mi. Quisque auctor odio sit amet est porttitor ornare. Suspendisse semper efficitur ultricies. Pellentesque faucibus enim ac ligula accumsan vulputate.",
                "thirdhead" => "Dolor",
                "thirdbody" => "Nam sollicitudin ante vel nibh eleifend fringilla. Phasellus ex ligula, consequat sit amet cursus semper, consequat ac dolor. Duis ultricies sem enim, quis euismod sapien ultrices id. Praesent et ipsum at leo congue rutrum. Vestibulum at velit et nisi interdum eleifend ac in nibh. Donec mauris dui, pulvinar sed quam id, scelerisque volutpat risus. Nunc quis scelerisque nulla, at dignissim enim. Duis eros lectus, scelerisque in imperdiet vitae, luctus vel arcu. Ut hendrerit lorem vel purus rhoncus dictum. Fusce scelerisque consequat metus nec blandit."
            ),
            8 => array(
                "large_text" => "Dit is een grote tekst",
                "small_text" => "Dit is een kleine tekst",
                "date" => "29 juni 2017",
                "category" => "varen",
                "link" => "/varen",
                "image" => "images/foto9.jpg",
                
                "summary" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam mauris urna, pellentesque vitae nisi at, efficitur placerat dolor. Curabitur at felis vestibulum, faucibus mi quis, consequat est. Curabitur hendrerit suscipit ante tempus sollicitudin. Aenean sed egestas est.",
                "readtime" => "2",
                "firsthead" => "Lorem",
                "firstbody" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dignissim sollicitudin dui ac vehicula. Integer id nibh sed ex rutrum condimentum. Duis a bibendum quam. Suspendisse maximus varius commodo. Nulla congue dapibus libero vitae efficitur. Maecenas molestie lorem massa, et luctus velit semper vel. Etiam faucibus tellus a lacus vehicula pulvinar. Vestibulum velit lacus, egestas ornare purus eu, feugiat hendrerit ipsum. Pellentesque venenatis sed arcu vel vulputate. Curabitur vitae scelerisque metus. Curabitur pretium sollicitudin felis eu commodo.",
                "secondhead" => "Ipsum",
                "secondbody" => "Donec fermentum, tortor sed rhoncus vehicula, metus nibh lobortis mauris, a fermentum sem erat ac mi. Etiam sit amet rutrum nisi. Etiam placerat elit ac sollicitudin rutrum. Etiam elit turpis, interdum nec orci vel, mattis gravida lectus. Proin tempus, tellus vitae vulputate placerat, dolor arcu lobortis lectus, congue lobortis ante mi sed augue. Vivamus orci orci, pharetra nec leo non, posuere posuere mi. Quisque auctor odio sit amet est porttitor ornare. Suspendisse semper efficitur ultricies. Pellentesque faucibus enim ac ligula accumsan vulputate.",
                "thirdhead" => "Dolor",
                "thirdbody" => "Nam sollicitudin ante vel nibh eleifend fringilla. Phasellus ex ligula, consequat sit amet cursus semper, consequat ac dolor. Duis ultricies sem enim, quis euismod sapien ultrices id. Praesent et ipsum at leo congue rutrum. Vestibulum at velit et nisi interdum eleifend ac in nibh. Donec mauris dui, pulvinar sed quam id, scelerisque volutpat risus. Nunc quis scelerisque nulla, at dignissim enim. Duis eros lectus, scelerisque in imperdiet vitae, luctus vel arcu. Ut hendrerit lorem vel purus rhoncus dictum. Fusce scelerisque consequat metus nec blandit."
            ),
        );
        return $stories;
    }
}