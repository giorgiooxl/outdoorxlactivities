
function opennav() {
    document.body.className = 'opened';
}

function closenav() {
    document.body.className = 'normal';
}

function loadmore() {
    
    //var events = document.getElementsByClassName('event_holder');
    var events = $('.events .event_holder');
    var events_storage = $('#storage .event_holder');
    var j = 0;
    var i = 0;
    $.each(events, function(i, value) {
        //i++;
        if($(this).hasClass('.event_active'))
        {
            console.log(j);
            j++;
            if(j > 3)
            {
                return false;      
            }
            
        }
        $(this).addClass('event_active');
        //events_storage[i].addClass('event_active');
    });
    //for(var i = 0; i <= events.length -1; i++)
    //{
    //    if(events[i].hasClass('event_active'))
    //    {
    //        j++;
    //        if(j > 3)
    //        {
    //            break;       
    //        }
    //        
    //    }
    //    events[i].addClass('event_active');
    //    events_storage[i].addClass('event_active');
    //}
    if(i + 2 >= event_images) {
        document.getElementById("loadmore").style.display = "none";
    }
}

$(function()
{
    
    $('body').append('<div style="display: none" id="storage"></div>');
    $('#storage').html($('.events').html());
    
    $('.filter_menu li a').on('click', function(){
        
        var cat = $(this).data('category');
        
        var items = $('<div>').append($('#storage .'+cat)).html();
        $('.events').html(items);
        
        console.log(items);
    })
    
    
});

$(window).scroll(function(){
    if($(this).scrollTop() > 730) {
        $('#summary').css('position', 'fixed');
        $('#summary').css('left', '0');
        $('#summary').css('top', '-40px');
        $('#smallspacing').css('height', '269.5px');
    } else {
        $('#summary').css('position', 'relative');
        $('#smallspacing').css('height', '0px');
    }
});